package com.hiray

import android.databinding.DataBindingUtil
import android.databinding.DataBindingUtil.setContentView
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import com.hiray.databinding.ActivityUnderlyingmainBinding
import com.hiray.databinding.NavHeaderMainBinding
import com.hiray.di.component.DaggerMainComponent
import com.hiray.mvvm.viewmodel.UnderlyingMainViewModel
import com.hiray.mvvm.viewmodel.NetWorkViewModel
import com.hiray.mvvm.viewmodel.UserViewModel
import com.hiray.util.AppHelper
import com.hiray.util.Toasty
import kotlinx.android.synthetic.main.activity_underlyingmain.*
import javax.inject.Inject

class UnderlyingMainActivity : AppCompatActivity() {
    val TAG = "UnderlyingMainActivity"
    @Inject
    lateinit var mainViewModel: UnderlyingMainViewModel

    @Inject
    lateinit var networkViewModel: NetWorkViewModel

    @Inject
    lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerMainComponent.builder().appComponent((application as App).appComponent)
                .build().inject(this@UnderlyingMainActivity)
        val mainBinding = setContentView<ActivityUnderlyingmainBinding>(this, R.layout.activity_underlyingmain)
        var headerMainBinding = DataBindingUtil.inflate<NavHeaderMainBinding>(layoutInflater,
                R.layout.nav_header_main, mainBinding.navView, false)

        mainBinding.navView.addHeaderView(headerMainBinding.root)
        userViewModel.context = this
        headerMainBinding.userViewModel = userViewModel
        headerMainBinding.executePendingBindings()
        mainBinding.recyclerView.addItemDecoration(DividerItemDecoration())
        mainBinding.viewmodel = mainViewModel
        mainBinding.networkViewModel = networkViewModel
        mainBinding.networkErrorLayout!!.networkViewModel = networkViewModel
        mainBinding.networkErrorLayout.executePendingBindings()
        mainBinding.executePendingBindings()
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState()
        mainViewModel.start()
        val code = AppHelper.persistInvitationCode(this)
        if (code == null)
            Toasty.message(code.toString())
        else Toasty.message(code)
    }


    inner class DividerItemDecoration : RecyclerView.ItemDecoration() {
        private val mDivider: Drawable? = ContextCompat.getDrawable(this@UnderlyingMainActivity, R.drawable.divider)

        override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
            val left = parent.paddingLeft
            val right = parent.width - parent.paddingRight

            val childCount = parent.childCount
            for (i in 0 until childCount) {
                val child = parent.getChildAt(i)

                val params = child.layoutParams as RecyclerView.LayoutParams

                val top = child.bottom + params.bottomMargin
                val bottom = top + mDivider!!.intrinsicHeight

                mDivider.setBounds(left, top, right, bottom)
                mDivider.draw(c)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        userViewModel.dispose()
    }
}

