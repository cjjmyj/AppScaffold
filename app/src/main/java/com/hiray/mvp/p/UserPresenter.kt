package com.hiray.mvp.p

import com.hiray.di.ActivityScope
import com.hiray.mvp.v.AvatarView
import com.hiray.mvp.v.ModifyPasswordView
import com.hiray.mvvm.model.ResponseFunc
import com.hiray.mvvm.model.ResponseWrapper
import com.hiray.mvvm.model.RestApi
import com.hiray.mvvm.model.RestApiHelper
import com.hiray.ui.my.ModifyPasswordActivity
import io.reactivex.ObservableSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.MultipartBody.FORM
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject


@ActivityScope
class UserPresenter @Inject constructor(var restApi: RestApi) {

    var avatarView: AvatarView? = null
    var modifyPasswordView: ModifyPasswordView? = null


    //上传头像
    fun uploadAvatar(f: File) {
        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),f)
        val body = MultipartBody.Part.createFormData("avatar",f.name,requestBody)
        restApi.uploadAvatarPic(body)
                .flatMap(ResponseFunc())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError {

                }
                .doOnNext {
                    avatarView?.onAvatarChange(it)
                }
                .subscribe()

    }

    fun modifyPassword() {
//        restApi.modifyPassword()
    }

    fun getModifyPasswordCaptcha() {

    }

    //修改密码
}