package com.hiray.mvp.v


interface AuthView {
    fun onAuthExpired(e: Throwable)
}

interface AvatarView : AuthView {
    //    fun onAvatarUpload(avatarUrl: String)
    //更新成功，上传成功
    fun onAvatarChange(avatarUrl: String)
}


interface ModifyPasswordView {
    fun onPasswordModified()
}
