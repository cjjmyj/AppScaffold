package com.hiray.repository

import com.hiray.mvvm.model.entity.News
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.internal.operators.observable.ObservableCreate

interface INewsRepository {

    fun fetchNewsBefore(date: String): Observable<List<News>>

}

class NewsRepository : INewsRepository {

    override fun fetchNewsBefore(date: String): Observable<List<News>> {
        return ObservableCreate(ObservableOnSubscribe<List<News>> { _ ->


        })
    }

}