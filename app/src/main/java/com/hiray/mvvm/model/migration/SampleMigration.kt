package com.hiray.mvvm.model.migration

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.migration.Migration

//比如User增加一个字段 foo
class SampleMigration(startVersion: Int, endVersion: Int) : Migration(startVersion, endVersion) {

    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALERT TABLE User  " +
                "ADD COLUMN foo")
    }
}