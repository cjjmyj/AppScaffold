package com.hiray.mvvm.model

import com.hiray.mvvm.model.entity.News
import com.hiray.mvvm.model.entity.TopStory
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface RestApi {

    @POST("createUser")
    fun createUser(@Query("key") key: String,
                   @Query("phone") phone: String,
                   @Query("password") password: String)


    @GET("before/{date}")
    fun fetchNewsBefore(@Path("date") date: String): Observable<Response<News>>

    @GET("latest")
    fun fetchLatestNews(): Observable<LatestResponse<News, TopStory>>

//    fun fetchDiamondInfo() :Observable<Response<List<Float>>>

    //更改默认头像
    @POST("")
    fun changeDefaultAvatar(): Observable<ResponseWrapper<String>>

    //上传头像
    @POST("")
    @Multipart
    fun uploadAvatarPic(@Part part:MultipartBody.Part):Observable<ResponseWrapper<String>>
}