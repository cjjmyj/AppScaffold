package com.hiray.mvvm.model.entity.main


data class Recommedation(var title: String, var description: String = "", var shouldRecommend: Boolean = false)