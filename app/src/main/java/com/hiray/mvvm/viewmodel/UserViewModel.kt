package com.hiray.mvvm.viewmodel

import android.content.Context
import android.content.Intent
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableFloat
import com.hiray.R
import com.hiray.aop.net.NetWorkRequired
import com.hiray.event.LoginEvent
import com.hiray.mvvm.model.entity.main.Recommedation
import com.hiray.ui.LoginActivity
import com.hiray.util.Toasty
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import javax.inject.Inject


class UserViewModel @Inject constructor() {
    lateinit var context: Context

    var login = ObservableBoolean(false)
    var name = ObservableField<String>("登录")

    init {
        EventBus.getDefault().register(this)
    }

    val avatarUrl = ObservableField<String>()

    val userName = ObservableField<String>()

    val diamond = ObservableField<String>("0.01")

    val energy = ObservableField<String>("0.00")

    val hasNew = ObservableBoolean()

    @NetWorkRequired
    fun start() {
        avatarUrl.set("http://pic.52112.com/icon/256/20180917/21713/932430.png")
        userName.set("阿扎芭芭拉")
        diamond.set("125.22")
        energy.set("25.21")
        hasNew.set(true)
    }

    fun tryLogin() {
        if (login.get())
            Toasty.Companion.message(R.string.hi_msg)
        else context.startActivity(Intent(context, LoginActivity::class.java))
    }

    fun dispose() {
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onLoginEvent(event: LoginEvent) {
        //抓取用户数据,暂时这么写
        login.set(event.success)
        name.set(event.name)

    }


}