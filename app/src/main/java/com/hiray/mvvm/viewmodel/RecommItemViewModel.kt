package com.hiray.mvvm.viewmodel

import android.view.View
import com.hiray.util.Toasty

class RecommItemViewModel(val title: String,
                          val description: String,
                          val iconUrl: String,
                          val isRecomm: Boolean
        /*超链接等等属性*/) {

    fun onItemClick(v: View) {
        Toasty.message("clicked task!")
    }
}