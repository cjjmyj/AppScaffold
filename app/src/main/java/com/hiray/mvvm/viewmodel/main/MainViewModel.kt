package com.hiray.mvvm.viewmodel.main

import android.databinding.ObservableField
import android.view.View
import com.hiray.BR
import com.hiray.R
import com.hiray.aop.net.NetWorkRequired
import com.hiray.di.ActivityScope
import com.hiray.mvvm.model.RestApi
import com.hiray.mvvm.viewmodel.RecommItemViewModel
import com.hiray.util.Toasty
import com.hiray.widget.ShuffleLayout
import me.tatarka.bindingcollectionadapter2.ItemBinding
import javax.inject.Inject

@ActivityScope
class MainViewModel @Inject constructor(var restApi: RestApi) {
    private val TAG = "MainViewModel"
    //用val 修饰似乎更容易链接到xml 布局文件中对应的参数？？？
    val datas = ObservableField<List<Float>>()

//    val avatarUrl = ObservableField<String>("http://pic.52112.com/icon/256/20180917/21713/932430.png")
//
//    val userName = ObservableField<String>()

    val recommendationData: ObservableField<List<RecommItemViewModel>> = ObservableField()

    val recommedationItemBinding = ItemBinding.of<RecommItemViewModel>(BR.recomItemViewModel, R.layout.recommendation_item)

    @NetWorkRequired
    fun start() {
        datas.set(listOf(1.5f, 3f, 0.01f, 7f, 1f, 243f, 12f, 3f, 5f, 5f, 63f, 45f, 43f, 2f, 43f, 5f, 64f, 2f, 25f, 25f, 5f, 63f))
        recommendationData.set(listOf(
                RecommItemViewModel("新人任务1", "任务1", "", false),
                RecommItemViewModel("新人任务2", "任务2", "", false),
                RecommItemViewModel("新人任务3", "任务3", "", true),
                RecommItemViewModel("新人任务4", "任务4", "", false)))
    }


    fun onDiamondCollect(shuffleLayout: ShuffleLayout, index: Int, v: View) {
        Toasty.message("collect ${datas.get()!![index]}")
        shuffleLayout.expel(v)
    }
}