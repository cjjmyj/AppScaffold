package com.hiray.exception

/**
 * 登录信息过期或者失效
 */
class ExpiredException : Throwable {
    constructor(message: String?) : super(message)
}