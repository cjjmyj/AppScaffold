package com.hiray.di

interface ComponentProvider<T> {

   fun getComponent():T
}