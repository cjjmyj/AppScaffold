package com.hiray.di.component

import com.hiray.di.ActivityScope
import com.hiray.ui.my.ModifyPasswordActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class])
open interface AccountComponent {

    fun inject(activity: ModifyPasswordActivity)
}