package com.hiray.di.component

import com.hiray.UnderlyingMainActivity
import com.hiray.di.ActivityScope
import com.hiray.ui.MainActivity
import com.hiray.ui.main.MainFragment
import com.hiray.ui.main.MyFragment
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class])
interface MainComponent {
    fun inject(activity: UnderlyingMainActivity)
    fun inject(activity: MainActivity)

    fun inject(fragment: MainFragment)
    fun inject(myFragment: MyFragment)
}