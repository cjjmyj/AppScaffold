package com.hiray.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.widget.FrameLayout
import com.hiray.R

class HexagonBackgroundLayout : FrameLayout {

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

    }

    val s = 1.732f
    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        canvas.drawBitmap(BitmapFactory.decodeResource(resources, R.drawable.hexagon), 0f, 0f, Paint())
    }
}