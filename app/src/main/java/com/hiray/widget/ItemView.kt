package com.hiray.widget

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import com.hiray.R

class ItemView(context: Context?) : FrameLayout(context) {

    fun setDiamondProfit(some: Float) {
        findViewById<TextView>(R.id.diamond_profit).text = some.toString()
    }

    init {
        val layoutInflater = LayoutInflater.from(context)
        val itemView = layoutInflater.inflate(R.layout.item_layout, null)
        addView(itemView)
    }
}
