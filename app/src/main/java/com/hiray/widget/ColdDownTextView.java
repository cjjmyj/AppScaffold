package com.hiray.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.annotation.ColorInt;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;

import com.hiray.R;

public class ColdDownTextView extends AppCompatTextView {
    private static final String TAG = "ColdDownTextView";
    @ColorInt
    int idleTextColor;
    @ColorInt
    int coldingTextColor;
    int coldTime;
    String unit;
    String idleText;

    boolean isCounting = false;
    private CountingDownTimer countingDownTimer;

    public ColdDownTextView(Context context) {
        super(context);
    }

    public ColdDownTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ColdDownTextView);
        idleText = array.getString(R.styleable.ColdDownTextView_idle_text);
        idleTextColor = array.getColor(R.styleable.ColdDownTextView_idle_text_color, Color.TRANSPARENT);
        coldingTextColor = array.getColor(R.styleable.ColdDownTextView_coldding_text_color, Color.WHITE);
        coldTime = array.getInt(R.styleable.ColdDownTextView_cold_time, 60);
        unit = array.getString(R.styleable.ColdDownTextView_unit);
        setText(idleText);
        setTextColor(idleTextColor);
        array.recycle();
    }

    public ColdDownTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void start() {
        if (!isCounting) {
            Log.i(TAG, "onClick: start!");
            countingDownTimer = new CountingDownTimer(coldTime * 1000, 1000);
            countingDownTimer.start();
            setTextColor(coldingTextColor);
        }
    }

    public boolean isCounting() {
        return isCounting;
    }

    @SuppressWarnings("SetTextI18n")
    private class CountingDownTimer extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public CountingDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            isCounting = true;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            Log.i(TAG, "onTick: ");
            setText(millisUntilFinished / 1000 + unit + "后重发");
        }

        @Override
        public void onFinish() {
            isCounting = false;
            setTextColor(idleTextColor);
            setText(idleText);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (countingDownTimer != null)
            countingDownTimer.cancel();
    }
}
