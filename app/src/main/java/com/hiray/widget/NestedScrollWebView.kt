package com.hiray.widget

import android.content.Context
import android.support.v4.view.NestedScrollingChild
import android.support.v4.view.NestedScrollingChildHelper
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.MotionEvent.*
import android.view.View
import android.view.ViewConfiguration
import android.webkit.WebView

/**
 * WebView自身并不支持嵌套滚动，这里让WebView继承NestedScrollingChild，让其可以支持嵌套滚动，可以和其他View一起
 * 依靠嵌套滚动实现一些依靠嵌套滚动实现的效果
 */
class NestedScrollWebView : WebView, NestedScrollingChild {
    private val TAG = "NestedScrollWebView"
    private var nestedScrollingChildHelper: NestedScrollingChildHelper
    private var lastY = 0f
    private var mTouchSlop: Int

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        isNestedScrollingEnabled = true
        nestedScrollingChildHelper = NestedScrollingChildHelper(this)
        mTouchSlop = ViewConfiguration.get(context).scaledTouchSlop
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        var dy: Int
        val consumed = IntArray(2)
        if (event.actionMasked == ACTION_DOWN) {
            lastY = event.y
            startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL)
            return true
        }
        if (event.actionMasked == ACTION_MOVE) {
            dy = (lastY - event.y).toInt()
            nestedScrollingChildHelper.dispatchNestedPreScroll(0, dy.toInt(), consumed, null)
            dy -= consumed[1]

            if (dy > 0)
                dy -= mTouchSlop
            else dy += mTouchSlop
            scrollInternal(dy)
            //消耗自己应该消耗的，记录消耗和剩余未消耗的
//            nestedScrollingChildHelper.dispatchNestedScroll(0, dyUnconsumed)
        }




        if (event.actionMasked == ACTION_UP)
            Log.i(TAG, "onTouchEvent:height: ${height} range: " + computeVerticalScrollRange() + " offset: ${computeVerticalScrollOffset()}" +
                    "extent: ${computeVerticalScrollExtent()}")


        when (event.actionMasked) {
            ACTION_UP, ACTION_POINTER_DOWN, ACTION_CANCEL -> stopNestedScroll()
        }
        return false
    }

    private fun scrollInternal(dy: Int) {
        val scrolledOffset = computeVerticalScrollOffset()
        val scrollRange = computeVerticalScrollRange()
        val extent = computeVerticalScrollExtent()
        var scrollUnconsumed: Int
        var scrollConsumed: Int
        if (dy < 0) {
            scrollConsumed = Math.max(dy, -scrolledOffset)
            scrollBy(0, dy)
            scrollUnconsumed = Math.max(0, dy - scrollConsumed)
            dispatchNestedScroll(0, scrollConsumed, 0, scrollUnconsumed, null)
        }else{
            scrollConsumed = Math.min(dy,scrollRange - extent)
            scrollBy(0,dy)
            scrollUnconsumed = Math.min(0,dy - scrollConsumed)
            dispatchNestedScroll(0, scrollConsumed, 0, scrollUnconsumed, null)
        }

    }


    override fun isNestedScrollingEnabled(): Boolean {
        return nestedScrollingChildHelper.isNestedScrollingEnabled
    }

    override fun startNestedScroll(axes: Int): Boolean {
        return nestedScrollingChildHelper.startNestedScroll(axes)
    }

    override fun dispatchNestedPreScroll(dx: Int, dy: Int, consumed: IntArray?, offsetInWindow: IntArray?): Boolean {
        return nestedScrollingChildHelper.dispatchNestedPreScroll(dx, dy, consumed, offsetInWindow)
    }

    override fun dispatchNestedScroll(dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int, offsetInWindow: IntArray?): Boolean {
        return nestedScrollingChildHelper.dispatchNestedScroll(dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, offsetInWindow)
    }

    override fun stopNestedScroll() {
        nestedScrollingChildHelper.stopNestedScroll()
    }


}