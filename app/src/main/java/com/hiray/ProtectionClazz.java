package com.hiray;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

/*仅仅用来生成错误的字节码插入dex 以此达到阻止反编译的目的*/
public class ProtectionClazz {
    public boolean aBoolean(int ori) {
        return false;
    }

    public int aInt() {
        return 2;
    }

    public void startActivity(Class<?> clazz){
        Context context = null;
        context.startActivity(new Intent(context,clazz));
    }
}
