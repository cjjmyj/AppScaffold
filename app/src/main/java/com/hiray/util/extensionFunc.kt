package com.hiray.util

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

fun Any.createFragmentPagerAdapter(titles: List<String>, fm: FragmentManager, fragments: List<Fragment>): FragmentPagerAdapter {

    return object : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int) = fragments[position]

        override fun getCount() = fragments.size

        override fun getPageTitle(position: Int) = titles[position]

    }
}

fun Fragment.quickStartActivity(clazz: Class<*>) = this.startActivity(Intent(this.context, clazz))

fun FragmentActivity.quickStartActivity(clazz: Class<*>) = this.startActivity(Intent(this, clazz))