package com.hiray.util

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.util.Log
import com.hiray.util.AppHelper.Helper.context
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException


class ShareHelper {
    // 微信好友
    // package = com.tencent.mm,
    // activity = com.tencent.mm.ui.tools.ShareImgUI
    // 微信朋友圈
    // package = com.tencent.mm,
    // activity = com.tencent.mm.ui.tools.ShareToTimeLineUI
    //
    // package = com.tencent.mobileqq,
    // activity = com.tencent.mobileqq.activity.JumpActivity
    // package = com.tencent.mobileqq,
    // activity = com.tencent.mobileqq.activity.qfileJumpActivity
    // QQ空间
    // package = com.qzone, activity =
    // com.qzone.ui.operation.QZonePublishMoodActivity
    // 人人
    // package = com.renren.mobile.android,
    // activity = com.renren.mobile.android.publisher.UploadPhotoEffect
    // 陌陌
    // package = com.immomo.momo, activity =
    // com.immomo.momo.android.activity.feed.SharePublishFeedActivity
    // 新浪微博
    // package = com.sina.weibo, activity = com.sina.weibo.EditActivity

    companion object ShareHelperSOUL {

        private fun saveBitmap(bm: Bitmap, picName: String): String {
            try {
                val path = context.cacheDir.absolutePath + File.separator + picName
                val f = File(path)
                if (!f.exists()) {
                    f.parentFile.mkdirs()
                    f.createNewFile()
                }

                val out = FileOutputStream(f)
                bm.compress(Bitmap.CompressFormat.PNG, 90, out)
                out.flush()
                out.close()
                Log.i("999", "保存成功：path=$path")
                return f.absolutePath
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return ""
        }

        fun isInstalled(context: Context, packageName: String, activityName: String): Boolean {

            val intent = Intent()
            intent.component = ComponentName(packageName, activityName)
            val list = context.packageManager.queryIntentActivities(intent, 0)
            return list.size > 0
        }

    }
}