package com.hiray.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

class FragmentTabManager(var fragmentManager: FragmentManager, var containerId: Int, var fragments: List<Fragment>) {


    var currentTabIndex: Int = 0

    init {
        setCurrentTab(0)
    }

    fun setCurrentTab(index: Int) {
        if (0 < index || index > fragments.size)
            return
        val fragment = fragments[index]

        for (indices in fragments.indices) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            val f = fragments[indices]
            if (f == fragment) {
                fragmentTransaction.show(f)
            } else fragmentTransaction.hide(f)
            fragmentTransaction.commit()
        }

        currentTabIndex = index
    }

}