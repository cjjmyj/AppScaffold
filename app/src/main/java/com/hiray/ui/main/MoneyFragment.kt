package com.hiray.ui.main


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.hiray.R
import com.hiray.ui.BaseFragment
import com.hiray.ui.FragmentChangeListener
import com.hiray.ui.StatusbarUtil
import com.hiray.ui.money.FastMoneyFragment
import com.hiray.ui.money.LoanFragment
import com.hiray.ui.money.TryPlayingFragment
import com.hiray.util.createFragmentPagerAdapter
import kotlinx.android.synthetic.main.fragment_money.*

/**
 * A simple [BaseFragment] subclass.
 *
 */
class MoneyFragment : BaseFragment() {
    private val TAG = "MoneyFragment"
    lateinit var statusBarHandler: FragmentChangeListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_money, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewPager.adapter = createFragmentPagerAdapter(
                listOf("试玩", "快赚", "高额"),
                childFragmentManager,
                listOf(TryPlayingFragment(), FastMoneyFragment(), LoanFragment()))
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        statusBarHandler = context as FragmentChangeListener
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden)
            statusBarHandler.setStatusBarColor(R.color.colorPrimary)
    }
}
