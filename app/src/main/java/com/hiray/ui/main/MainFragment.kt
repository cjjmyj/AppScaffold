package com.hiray.ui.main


import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hiray.R
import com.hiray.databinding.FragmentMainBinding
import com.hiray.di.component.MainComponent
import com.hiray.mvvm.viewmodel.UserViewModel
import com.hiray.mvvm.viewmodel.main.MainViewModel
import com.hiray.ui.BaseFragment
import com.hiray.ui.FragmentChangeListener
import javax.inject.Inject


class MainFragment : BaseFragment() {
    private val TAG = "MainFragment"
    private lateinit var fragmentMainBinding: FragmentMainBinding

    @Inject
    lateinit var userViewModel: UserViewModel

    @Inject
    lateinit var mainViewModel: MainViewModel

    lateinit var statusBarHandler: FragmentChangeListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val component = getComponent(MainComponent::class.java)
        component.inject(this)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragmentMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        return fragmentMainBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        fragmentMainBinding.mainViewModel = mainViewModel
        fragmentMainBinding.userViewModel = userViewModel
        userViewModel.start()
        mainViewModel.start()
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        statusBarHandler = context as FragmentChangeListener
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden)
            statusBarHandler.setStatusBarColor(R.color.colorPlanetBg)
    }
}
