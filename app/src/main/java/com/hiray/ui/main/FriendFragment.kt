package com.hiray.ui.main


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import com.bumptech.glide.Glide
import com.hiray.R
import com.hiray.ui.BaseFragment
import com.hiray.util.Store
import com.hiray.util.Toasty
import kotlinx.android.synthetic.main.fragment_friend.*
import kotlinx.android.synthetic.main.invite_dialog_layout.*
import java.io.File


/**
 * A simple [BaseFragment] subclass.
 *
 */
class FriendFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_friend, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val settings = friendWebView.settings
        settings.javaScriptEnabled = true
        settings.useWideViewPort = true
        settings.setAppCacheEnabled(true)
        settings.setAppCachePath(context!!.cacheDir.toString())
        friendWebView.webChromeClient = WebChromeClient()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initWebView()
        friendWebView.loadUrl(Store.URL_ACTIVITY)
        inviteImmediately.setOnClickListener {
            val rootView = layoutInflater.inflate(R.layout.invite_dialog_layout, null)
            val shareDialog = AlertDialog.Builder(activity!!)
                    .setView(rootView)
                    .create()
            rootView.findViewById<View>(R.id.closeBtn).setOnClickListener { shareDialog.dismiss() }
            rootView.findViewById<View>(R.id.share_wechat).setOnClickListener {
                Glide.with(rootView).load(File(Environment.getExternalStorageDirectory(), "share.png")).into(wechat_img)
//                Toasty.message("微信")
            }
            rootView.findViewById<View>(R.id.share_timeline).setOnClickListener { Toasty.message("朋友圈") }
            rootView.findViewById<View>(R.id.share_qq).setOnClickListener {
                Toasty.message("QQ")
            }
            rootView.findViewById<View>(R.id.share_qrcode).setOnClickListener { Toasty.message("二维码") }
            rootView.findViewById<View>(R.id.share_SB).setOnClickListener { Toasty.message("微博") }
            rootView.findViewById<View>(R.id.share_qqzone).setOnClickListener { Toasty.message("QQ空间") }


            shareDialog.show()

        }
    }


}
