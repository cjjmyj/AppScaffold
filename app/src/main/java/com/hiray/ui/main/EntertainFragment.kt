package com.hiray.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import com.hiray.R
import com.hiray.ui.BaseFragment
import com.hiray.ui.FragmentChangeListener
import kotlinx.android.synthetic.main.activity_web_view.*
import kotlinx.android.synthetic.main.fragment_entertain.*


class EntertainFragment : BaseFragment() {
    private val TAG = "EntertainFragment"
    private lateinit var statusBarHandler: FragmentChangeListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_entertain, container, false)
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        statusBarHandler = context as FragmentChangeListener
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden)
            statusBarHandler.setStatusBarColor(R.color.colorPlanetBg)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initWebView()
        readWebView.loadUrl("https://m.douban.com")
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val settings = readWebView.settings
        settings.javaScriptEnabled = true
        settings.useWideViewPort = true
        settings.setAppCacheEnabled(true)
        settings.setAppCachePath(context!!.cacheDir.toString())
        readWebView.webViewClient = WebViewClient()
        readWebView.webChromeClient = WebChromeClient()

    }
}
