package com.hiray.ui.my.withdraw


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hiray.R
import com.hiray.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_withdraw.*


class WithdrawFragment : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_withdraw, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val withDrawAdapter = WithDrawAdapter()
        withDrawAdapter.setDatas(arrayListOf("50元", "100元", "200元", "500元"))
        recyclerView.adapter = withDrawAdapter
    }


}
