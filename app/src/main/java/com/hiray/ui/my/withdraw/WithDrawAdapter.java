package com.hiray.ui.my.withdraw;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hiray.R;

import java.util.ArrayList;
import java.util.List;

public class WithDrawAdapter extends RecyclerView.Adapter<WithDrawAdapter.ViewHolder> {


    private List<String> datas = new ArrayList<>(4);
    private int prevPos = 0;
    private int currPos = 0;
    private Context context;

    public void setDatas(List<String> datas) {
        if (datas != null && !datas.isEmpty()) {
            this.datas.clear();
            this.datas.addAll(datas);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (context == null)
            context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_withdraw_amount, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView;
        }

        public void bind() {
            textView.setText(datas.get(getAdapterPosition()));
            if (getAdapterPosition() == currPos)
                textView.setTextColor(ContextCompat.getColor(context, R.color.appTextColorPrimary));
            else textView.setTextColor(ContextCompat.getColor(context, R.color.appSubTextColor));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != currPos) {
                        prevPos = currPos;
                        currPos = getAdapterPosition();
                        ((AppCompatTextView) ((ViewGroup) itemView.getParent()).getChildAt(prevPos))
                                .setTextColor(ContextCompat.getColor(context, R.color.appSubTextColor));
                        textView.setTextColor(ContextCompat.getColor(context, R.color.appTextColorPrimary));
                    }
                }
            });
        }
    }
}
