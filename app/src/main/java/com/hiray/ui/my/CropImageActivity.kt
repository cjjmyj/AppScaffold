package com.hiray.ui.my

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.hiray.R
import com.isseiaoki.simplecropview.callback.CropCallback
import com.isseiaoki.simplecropview.callback.LoadCallback
import com.isseiaoki.simplecropview.callback.SaveCallback
import kotlinx.android.synthetic.main.activity_crop_image.*
import java.io.File

class CropImageActivity : AppCompatActivity() {

    private val TAG = "CropImageActivity"

    companion object {

        val REQUEST_CROP = 0xff00

        fun start(activity: Activity, uri: Uri) {
            val intent = Intent(activity, CropImageActivity::class.java)
            intent.putExtra("ImageUri", uri)
            activity.startActivityForResult(intent, REQUEST_CROP)
        }

        fun start(f: Fragment, uri: Uri) {
            val intent = Intent(f.activity, CropImageActivity::class.java)
            intent.putExtra("ImageUri", uri)
            f.startActivityForResult(intent, REQUEST_CROP)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crop_image)
        setSupportActionBar(toolbar)
        val uri = intent.getParcelableExtra<Uri>("ImageUri")
        cropImageView.startLoad(uri, object : LoadCallback {
            override fun onSuccess() {

            }

            override fun onError(e: Throwable?) {

            }

        })
    }

    private fun createSaveUri(): Uri {
        return Uri.fromFile(File(cacheDir, "cropped"))
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.crop_image_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.cropConfirm) {
            cropImageView.startCrop(createSaveUri(), CropCallbackImpl(), SaveCallBackImpl())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    //SaveCallback implementation
    inner class SaveCallBackImpl : SaveCallback {
        override fun onSuccess(uri: Uri) {
            val data = Intent()
            data.data = uri
            setResult(Activity.RESULT_OK, data)
            ActivityCompat.finishAfterTransition(this@CropImageActivity)
        }

        override fun onError(e: Throwable?) {
            Log.i(TAG, "onError: 保存裁切图片失败")
        }

    }

    //CropCallBack implementation
    inner class CropCallbackImpl : CropCallback {
        override fun onSuccess(cropped: Bitmap?) {

        }

        override fun onError(e: Throwable?) {
        }

    }

    //LoadCallBack implementation
    inner class LoadCallBackImpl : LoadCallback {
        override fun onSuccess() {

        }

        override fun onError(e: Throwable?) {
        }

    }
}
