package com.hiray.ui.my

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import com.hiray.R
import com.hiray.ui.StatusbarUtil
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)



        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        //不让内容顶到状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val content = (findViewById<ViewGroup>(android.R.id.content)).getChildAt(0)
            content?.fitsSystemWindows = true
        }

        StatusbarUtil.setStatusBarColor(this, android.R.color.transparent)

        captchaTextView.setOnClickListener {
            captchaTextView.start()
        }
    }
}
