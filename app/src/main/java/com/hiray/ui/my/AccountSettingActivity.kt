package com.hiray.ui.my

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.hiray.R
import com.hiray.ui.LoginActivity
import com.hiray.util.quickStartActivity
import kotlinx.android.synthetic.main.activity_account_setting.*

class AccountSettingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_setting)
        setSupportActionBar(toolbar)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }


    fun tryModifyPassword(v: View) {
        startActivity(Intent(this, ModifyPasswordActivity::class.java))
    }

    fun trySendFeedBack(v: View) {
        startActivity(Intent(this, FeedBackActivity::class.java))
    }

    /**
     * 登出
     */
    fun logout(v: View) {
        //清除登录信息

        quickStartActivity(LoginActivity::class.java)
    }
}
