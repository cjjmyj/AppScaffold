package com.hiray.ui.my

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.hiray.R
import com.hiray.ui.MainActivity
import com.hiray.ui.my.withdraw.WithdrawFragment
import kotlinx.android.synthetic.main.activity_my_detail.*

class MyDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_detail)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        //不让内容顶到状态栏
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            val content = (findViewById<ViewGroup>(android.R.id.content)).getChildAt(0)
//            content?.fitsSystemWindows = true
//        }
        //设置状态栏颜色
//        StatusbarUtil.setStatusBarColor(this, colorRes)

        flycoTabLayout.setTabData(arrayListOf(MainActivity.Tab.of("微信提现", 0, 0), MainActivity.Tab.of("支付宝提现", 0, 0)),
                this,
                R.id.my_detail_fragment_container, arrayListOf(WithdrawFragment(), WithdrawFragment()
        ))


    }
}
