package com.hiray.ui

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil.setContentView
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import com.hiray.App
import com.hiray.R
import com.hiray.di.component.DaggerLoginComponent
import com.hiray.di.module.AccountModule
import com.hiray.mvvm.viewmodel.LoginViewModel
import com.hiray.mvvm.viewmodel.NetWorkViewModel
import com.hiray.ui.my.ModifyPasswordActivity
import com.hiray.ui.my.RegisterActivity
import com.hiray.util.quickStartActivity
import javax.inject.Inject

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity() {
    private val TAG = "LoginActivity"

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, LoginActivity::class.java))
        }
    }

    @Inject
    lateinit var loginViewModel: LoginViewModel

    @Inject
    lateinit var networkViewModel: NetWorkViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        var loginComponent = DaggerLoginComponent
                .builder()
                .name(TAG)
                .appComponent((application as App).appComponent)
                .accountModule(AccountModule(this))
                .build()
        loginComponent
                .inject(this)
//        Log.i("NetWorViewModel_Ref", networkViewModel.toString())
        //全屏，同时保持状态栏
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        //不让内容顶到状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val content = (findViewById<ViewGroup>(android.R.id.content)).getChildAt(0)
            content?.fitsSystemWindows = true
        }

        StatusbarUtil.setStatusBarColor(this, android.R.color.transparent)

    }

    fun login(v: View) {

    }

    fun forgotPassword(v: View) {
        quickStartActivity(ModifyPasswordActivity::class.java)
        ActivityCompat.finishAfterTransition(this)
    }

    fun register(v: View) {
        quickStartActivity(RegisterActivity::class.java)
    }
}

