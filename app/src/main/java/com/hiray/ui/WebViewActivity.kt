package com.hiray.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.hiray.R
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : AppCompatActivity() {

    val REMOVE_MOBILE_HEADER_JS_SCRIPT = "javascript:(function() {" +
            "var item = document.getElementsByClassName(\'header-for-mobile\')[0];" +
            "item.parentNode.removeChild(item);" +
            "})()"

    companion object {
        fun load(context: Context, url: String) {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra("KEY_URL", url)
            context.startActivity(intent)
        }
    }


    private val webViewClient: WebViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            toolbar.title = view!!.title
            webView.loadUrl(REMOVE_MOBILE_HEADER_JS_SCRIPT)
        }
    }


    private val webChromeClient1: WebChromeClient = object : WebChromeClient() {
        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            if (newProgress >= 0)
                webView.loadUrl(REMOVE_MOBILE_HEADER_JS_SCRIPT)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        initWebView()
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val url = intent.getStringExtra("KEY_URL")
        if (url != null)
            webView.loadUrl(url)
        else finish()
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val settings = webView.settings
        settings.javaScriptEnabled = true
        settings.useWideViewPort = true
        settings.setAppCacheEnabled(true)
        settings.setAppCachePath(cacheDir.toString())

        var webviewClient: WebViewClient = webViewClient
        webView.webViewClient = webviewClient
        var webChromeClient: WebChromeClient = webChromeClient1
        webView.webChromeClient = webChromeClient

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
