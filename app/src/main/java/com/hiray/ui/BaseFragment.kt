package com.hiray.ui


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hiray.R
import com.hiray.di.ComponentProvider
import com.hiray.exception.ExpiredException
import com.hiray.mvp.v.AuthView
import com.hiray.util.quickStartActivity

/**
 * A simple [Fragment] subclass.
 *
 */
open class BaseFragment : Fragment(), AuthView {

    override fun onAuthExpired(t:Throwable) {
        if (t is ExpiredException)
            quickStartActivity(LoginActivity::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return TextView(activity).apply {
            setText(R.string.hello_blank_fragment)
        }
    }


    fun <T> getComponent(componentType: Class<T>): T {
        return componentType.cast((activity as ComponentProvider<*>).getComponent())
    }


}
