package com.hiray.ui

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.flyco.tablayout.listener.CustomTabEntity
import com.hiray.App
import com.hiray.R
import com.hiray.di.ComponentProvider
import com.hiray.di.component.DaggerMainComponent
import com.hiray.di.component.MainComponent
import com.hiray.mvvm.viewmodel.UserViewModel
import com.hiray.mvvm.viewmodel.main.MainViewModel
import com.hiray.ui.main.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), ComponentProvider<MainComponent>, FragmentChangeListener {

    override fun getComponent(): MainComponent {
        return mainComponent
    }

    @Inject
    lateinit var mainViewModel: MainViewModel

    @Inject
    lateinit var userViewModel: UserViewModel

    lateinit var mainComponent: MainComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainComponent = DaggerMainComponent.builder()
                .appComponent((application as App).appComponent)
                .build()
        setContentView(R.layout.activity_main)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_IMMERSIVE
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        flycoTabLayout.setTabData(arrayListOf(
                Tab.of("首页", R.drawable.ic_home_chcked, R.drawable.ic_home_unchecked),
                Tab.of("娱乐", R.drawable.ic_entertain_checked, R.drawable.ic_entertain_unchecked),
                Tab.of("赚钱", R.drawable.ic_money_checked, R.drawable.ic_money_unchecked),
                Tab.of("好友", R.drawable.friend_checked, R.drawable.friend_unchecked),
                Tab.of("我的", R.drawable.my_checked, R.drawable.my_unchecked)
        ),
                this@MainActivity,
                R.id.fragment_container,
                arrayListOf(
                        MainFragment(),
                        EntertainFragment(),
                        MoneyFragment(),
                        FriendFragment(),
                        MyFragment()
                ))
        flycoTabLayout.currentTab = 0


    }

    override fun setStatusBarColor(colorRes: Int) {
        //全屏，同时保持状态栏
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        //不让内容顶到状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val content = (findViewById<ViewGroup>(android.R.id.content)).getChildAt(0)
            content?.fitsSystemWindows = true
        }
        //设置状态栏颜色
        StatusbarUtil.setStatusBarColor(this, colorRes)
    }

    class Tab(private var title: String, private var selectedIcon: Int, private var unselectedIcon: Int) : CustomTabEntity {

        companion object G {
            fun of(t: String, s: Int, us: Int) = Tab(t, s, us)
        }

        override fun getTabUnselectedIcon(): Int {
            return unselectedIcon
        }

        override fun getTabSelectedIcon(): Int {
            return selectedIcon
        }

        override fun getTabTitle(): String {
            return title
        }

    }
}

interface FragmentChangeListener {
    fun setStatusBarColor(colorRes: Int)
}

