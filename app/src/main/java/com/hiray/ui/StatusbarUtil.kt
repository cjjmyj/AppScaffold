package com.hiray.ui

import android.content.Context
import android.os.Build
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.view.WindowManager

class StatusbarUtil {

    companion object StatusBarUtil {
        fun setStatusBarColor(activity: FragmentActivity, colorRes: Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(activity, colorRes)
            } /*else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                //使用SystemBarTint库使4.4版本状态栏变色，需要先将状态栏设置为透明
                transparencyBar(activity)
                val tintManager = SystemBarTintManager(activity)
                tintManager.setStatusBarTintEnabled(true)
                tintManager.setStatusBarTintResource(colorRes)
            }*/
        }

        fun getStatusbarHeight(context: Context): Int {
            var statusBarHeight = 0
            var identifier = context.resources.getIdentifier("status_bar_height", "dimen", "android")
            if (identifier != -1)
                statusBarHeight = context.resources.getDimensionPixelSize(identifier)

            return statusBarHeight
        }
    }
}