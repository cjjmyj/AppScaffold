package com.hiray

import android.app.Application
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.hiray.di.module.AppModule
import com.hiray.mvvm.model.Response
import com.hiray.mvvm.model.RestApi
import com.hiray.mvvm.model.RestApiHelper
import com.hiray.mvvm.model.entity.News
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.junit.After

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import java.text.SimpleDateFormat
import java.util.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.hiray", appContext.packageName)
    }


    lateinit var app: Application
    lateinit var appModule: AppModule
    lateinit var restApi: RestApi
    var sdf = SimpleDateFormat("yyyyMMdd")


    @Before
    fun initDep() {
        app = InstrumentationRegistry.getContext().applicationContext as Application
        appModule = AppModule(app)
        restApi = RestApiHelper(appModule.provideGson(), appModule.tslProvider()).create()
    }

    @After
    fun afterNoOp() {

    }

    @Test
    fun fetchNewBeforeTest() {
        val date = sdf.format(Date())
        restApi.fetchNewsBefore(date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<Response<News>> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: Response<News>) {
                        assert(t.news.isNotEmpty())
                    }

                    override fun onError(e: Throwable) {

                    }

                })
    }
}
