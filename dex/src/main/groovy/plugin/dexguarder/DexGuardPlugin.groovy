package plugin.dexguarder

import com.android.build.gradle.AppExtension
import org.gradle.api.Plugin
import org.gradle.api.Project

class DexGuardPlugin implements Plugin<Project> {

    def AppPlugin = "com.android.application"

    @Override
    void apply(Project project) {

        def isApp = project.plugins.hasPlugin(AppPlugin)
        if (isApp) {
            def android = project.extensions.getByType(AppExtension)
            android.registerTransform(new DexGuardTransform(project))

        } else project.logger.error("DexGuarder plugin cannot applied to non-android project")

    }
}