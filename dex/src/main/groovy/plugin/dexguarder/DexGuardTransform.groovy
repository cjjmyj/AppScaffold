package plugin.dexguarder

import com.android.build.api.transform.*
import com.android.build.gradle.internal.pipeline.TransformManager
import org.gradle.api.Project

import java.util.function.Consumer

class DexGuardTransform extends Transform {
    Project project

    DexGuardTransform(project) {
        this.project = project
    }

    @Override
    String getName() {
        return "HirayClayDexGuarderTransform"
    }

    @Override
    Set<QualifiedContent.ContentType> getInputTypes() {
        return TransformManager.CONTENT_CLASS
    }

    @Override
    Set<? super QualifiedContent.Scope> getScopes() {
        return TransformManager.SCOPE_FULL_PROJECT
    }

    @Override
    boolean isIncremental() {
        return false
    }

    @Override
    void transform(TransformInvocation transformInvocation) throws TransformException, InterruptedException, IOException {
        super.transform(transformInvocation)
        transformInvocation.inputs.forEach(new Consumer<TransformInput>() {
            @Override
            void accept(TransformInput transformInput) {
                println "jarInputs: " + transformInput.jarInputs
                println "directoryInputs: " + transformInput.directoryInputs

            }
        })
    }
}